<?php
/**
 * @file
 * Журналирует посетителей веб-сайта
 * @version 1.0-beta3
 */

// Ajax Debugging
require_once('FirePHPCore/FirePHP.class.php');
require_once('FirePHPCore/fb.php');

require_once('include/common.inc.php');

/**
 * Класс клиента
 */
class Client extends DB_Connect {
  private $key;
  private $ip;
  private $reqTime;
  private $id;
  private $new;

  public function __construct($client_id, $ip) {
    parent::__construct();
    $this->new = FALSE;
    $this->ip = $ip;
    $this->reqTime = REQUEST_TIME;
    $this->setkey($client_id);
    $this->id = $this->getClientId($this->key);
  }

  public function isNew() {
    return $this->new;
  }

  private function setKey($client_id) {
    if (!$client_id) {
      $this->newClient();
    }
    elseif (!$this->getClientId($client_id)) {
      $this->newClient();
    }
    else {
      $this->key = $client_id;
    }
  }

  /**
   * Проверяет выдавался ли этот идентификатор клиенту
   * Возвращает номер в случае удачи
   */
  private function getClientId($client_id) {
    $q = "SELECT id FROM clients WHERE client_key = :client_id;";
    $st = $this->db->prepare($q);
    $st->bindParam(':client_id', $client_id, PDO::PARAM_STR);
    $st->execute();
    $result = $st->fetchColumn();
    ////FB::log($result);
    return $result;
  }

  private function newClient() {
    $key = get_random();
    try {
      $sql = "INSERT INTO clients (client_key, first_ip, regtime, last_visit)
                VALUES (:client_key, :first_ip, :regtime, :last_visit);";
      $st = $this->db->prepare($sql);
      
      // Ожидаемые параметры
      $params = array(
        'client_key' => PDO::PARAM_STR,
        'first_ip' => PDO::PARAM_STR,
        'regtime' => PDO::PARAM_INT,
        'last_visit' => PDO::PARAM_INT,
      );
      // Данные для вставки
      $client = array(
        'client_key' => $key,
        'first_ip' => $this->ip,
        'regtime' => $this->reqTime,
        'last_visit' => $this->reqTime,
      );
      foreach ($params as $param => $type) {
        if (array_key_exists($param, $client)) {
          $st->bindParam(":{$param}", $client[$param], $type);
        }
      }
      // Выполним запрос    
      $result = $st->execute();
      //FB::log($st->errorInfo());
      $st->closeCursor();
      // Поставим куку
      setcookie(COOKIE_NAME, $key);
      // Установим текущий ключ
      $this->key = $key;
      // Новый клиент
      $this->new = TRUE;
      ////FB::log($key, 'newkey');
    }
    catch (Exception $e) {
      if (DEBUG) {
        die($e->getMessage());
      }
      else {
        die();
      }
    }
  }

  public function visit() {
    try {
      $q = "SELECT last_used FROM client_ips
        WHERE client_id = :client_id AND ip = :ip;";
      $st = $this->db->prepare($q);
      $st->bindParam(':client_id', $this->id, PDO::PARAM_INT);
      $st->bindParam(':ip', $this->ip, PDO::PARAM_STR);
      $st->execute();
      $last_used = $st->fetchColumn();
      //FB::log($last_used, __FUNCTION__);
      if (!$last_used) {
        $sql = "INSERT INTO client_ips (client_id, ip, last_used)
                  VALUES (:client_id, :ip, :last_used);";
      }
      else {
        $sql = "UPDATE client_ips SET last_used = :last_used
                  WHERE client_id = :client_id AND ip = :ip;";
      }

      $st = $this->db->prepare($sql);
      // Ожидаемые параметры
      $params = array(
        'client_id' => PDO::PARAM_INT,
        'ip' => PDO::PARAM_STR,
        'last_used' => PDO::PARAM_INT,
      );
      // Данные для вставки
      $client = array(
        'client_id' => $this->id,
        'ip' => $this->ip,
        'last_used' => $this->reqTime,
      );
      foreach ($params as $param => $type) {
        if (array_key_exists($param, $client)) {
          $st->bindParam(":{$param}", $client[$param], $type);
        }
      }
      // Выполним запрос    
      $result = $st->execute();
      //FB::log($st->errorInfo());
      $st->closeCursor();

      if (!$this->isNew()) {
        $sql = "UPDATE clients SET last_visit = :last_visit
                  WHERE id = :client_id;";
        $st = $this->db->prepare($sql);
        // Ожидаемые параметры
        $params = array(
          'client_id' => PDO::PARAM_INT,
          'last_visit' => PDO::PARAM_INT,
        );
        // Данные для вставки
        $client = array(
          'client_id' => $this->id,
          'last_visit' => $this->reqTime,
        );
        foreach ($params as $param => $type) {
          if (array_key_exists($param, $client)) {
            $st->bindParam(":{$param}", $client[$param], $type);
          }
        }
        // Выполним запрос    
        $result = $st->execute();
        //FB::log($st->errorInfo());
        $st->closeCursor();
      }
    }
    catch (Exception $e) {
    
    }
  }

}

abstract Class JDebug {
  public static function process() {
    // Запускаем сэссию
    fakesess();

    $client_id = !empty($_COOKIE[COOKIE_NAME]) ? $_COOKIE[COOKIE_NAME] : FALSE;
    $client_ip = $_SERVER['REMOTE_ADDR'];

    $client = new Client($client_id, $client_ip);
    //FB::log($client);
    $client->visit();
  }
}

JDebug::process();


