<?php

define('REQUEST_TIME', time());
define('SALT', sha1('jvnfvfnuv8hf0vhermf7m*(*&Y(*&hym8vf987y0y7mdsviuNBJKhbn'));
define('COOKIE_NAME', md5('dthnrthbstrsrhyjk67u5u8U978y&Y87YN98y8y7(y&*n078n'));
define('DEBUG', TRUE);

/**
 * Возвращает случайную строку
 */
function get_random() {
  return md5(uniqid(mt_rand().time().SALT, TRUE));
}

/**
 * Запускает дополнительную сэссию
 */
function fakesess() {
  if (!session_name() || session_name() == 'PHPSESSID') {
    session_name(md5('JDEBUGSESSID'));
  }
  session_start();
  $_SESSION['lastreqtime'] = REQUEST_TIME;
}

/**
 * Класс связи с БД
 */
class DB_Connect {
  const HOST = 'localhost';
  const BASE = 'jdebug';
  const USER = 'jdebug';
  const PASS = 'jdebug_pass';
  //const SUFFIX = '';

  protected $db;

  protected function __construct($db = NULL) {
    if (is_object($db)) {
      $this->db = $db;
    }
    else {
      $dsn = "mysql:host=".self::HOST.";dbname=".self::BASE;
      try {
        $this->db = new PDO($dsn, self::USER, self::PASS);
        
      }
      catch (Exception $e) {
        if (DEBUG) {
          die($e->getMessage());
        }
        else {
          die();
        }
      }
    }
  }

  public function getConn() {
    return $this->db;
  }
}

abstract class Auth {
  public static function request() {
    header('WWW-Authenticate: Basic realm="My Realm"');
    header('HTTP/1.0 401 Unauthorized');
    die('<b> 401 Unauthorized </b>');
  }

  public static function getUser($key) {
    $users = array(
      //'0' => array(
      //  'uid' => 0,
      //  'name' => 'Anonymous',
      //  'pass' => NULL,
      //),
      '1' => array(
        'uid' => 1,
        'name' => 'Vester',
        'pass' => sha1('foobar'),
      ),
    );

    if (is_int($key) && array_key_exists($key, $users)) {
      return $users[$key];
    }
    else {
      foreach ($users as $uid => $user) {
        if ($user['name'] == $key) {
          return $user;
        }
      }
      return FALSE;
    }
  }

  public static function process() {
    global $user;
    if (isset($_SESSION['uid'])) {
      $user = self::getUser($_SESSION['uid']);
    }
    elseif(!isset($_SESSION['uid']) && !isset($_SERVER['PHP_AUTH_USER'])) {
      self::request();
    }
    elseif (isset($_SERVER['PHP_AUTH_USER']) && self::valid($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW'])) {
      $user = self::getUser($_SERVER['PHP_AUTH_USER']);
      $_SESSION['uid'] = $user['uid'];
    }
    else {
      self::request();
    }
  }

  public static function valid($name, $pass) {
    $user = self::getUser($name);
    return $user && $user['pass'] == sha1($pass);
  }

  public static function logout() {
    session_unset();
    session_destroy();
  }
}

/**
 * Примитивный шаблонизатор
 */
class Tpl {
  private $tplName;
  private $ns;
  private $vars = array();

  public function __construct($tpl_name, $ns = FALSE) {
    $this->tplName = $tpl_name;
  }

  public function setVar($var, $content, $force = FALSE) {
    if (array_key_exists($var, $this->vars) && !$force) {
      throw new Exception('Var already exists');
    }
    else {
      $this->vars[$var] = $content;
    }
  }

  private function p($var) {
    if (array_key_exists($var, $this->vars)) {
      print $this->vars[$var];
    }
  }

  private function getTplPath() {
    $path = 'tpl/';
    $path .= $this->ns ? $this->ns : '';
    return $path;
  }

  private function getTplName() {
    return $this->tplName .'.tpl.php';
  }

  public function render() {
    extract($this->vars);
    $tpl = $this->getTplPath().'/'.$this->getTplName();
    include($tpl);
  }
}
