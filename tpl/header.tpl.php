<!DOCTYPE html>
<html>
<head>
  <title>jDebug</title>
  <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
  <? foreach($_CSS as $file): ?>
    <?=  "<link rel=\"stylesheet\" type=\"text/css\" href=\"{$file}\" />" ?>
  <? endforeach; ?>
  <? foreach($_JS as $file): ?>
    <?=  "<script type=\"text/javascript\" src=\"{$file}\"></script>" ?>
  <? endforeach; ?>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
  <script type="text/javascript">
    jQuery.noConflict();
  </script>
</head>
<body>

