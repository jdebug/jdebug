<?php

require_once('FirePHPCore/FirePHP.class.php');
require_once('FirePHPCore/fb.php');
require_once('include/common.inc.php');

abstract class Index {
  public static function process() {
    // Запускаем сэссию
    fakesess();
    // Требуем представиться
    Auth::process();

    global $body;
    //$_SESSION['foo'] = 'bar';
    //Auth::logout();
    global $user;
    FB::log($_SESSION);
    FB::log($user);
    $body->setVar('hello', "<b>Hello {$user['name']}!</b>");
  }
}

$_CSS = array(
  'css/style.css',
);
$_JS = array(
);

$body = new Tpl('index');
Index::process();

include('tpl/header.tpl.php');
$body->render();
include('tpl/footer.tpl.php');
